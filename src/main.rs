fn main() {
    println!("Hello, world!");
    // let product: Product;
}
// enum Size {
//     liquide { amount: f64, unit}
// }

struct Dimensions {
    length: f64,
    width: f64,
    height: f64,
}
enum Size {
    // Liter { amount: f64, unit: (ml, cl) },
    Weight(u64),
    Volume(u64),
}
enum Status {
    Draft,
    Proposed,
    Published,
    Rejected,
}
struct Collection {
    title: String,
    slug: String,
    status: Status,
    products: Vec<Product>,
}
struct Category {
    title: String,
    slug: String,
    status: Status,
    sub_categories: Vec<SubCategory>,
    products: Vec<Product>,
}
struct SubCategory {
    title: String,
    slug: String,
    status: Status,
    parent_category: Category,
    products: Vec<Product>,
}
enum Options {
    Colors(Vec<String>),
    Sizes(Vec<Size>),
    Materials(Vec<String>),
}
struct ProductTransaction {
    id: u64,
    time_stamp: std::time::Instant,
    order_id: u64, // TODO: relate to order id
    qty: u64,
}
struct ProductStats {
    sales: Vec<ProductTransaction>,
    returns: Vec<ProductTransaction>,
}
struct Media {
    id: u64,
    title: String,
    description: String,
    alt_text: String,
}
struct VariantImages {
    cut_out_id: u64,
    use_context_id: u64,
    vital_feature_id: u64,
    compatibility_id: u64,
    all_parts_id: u64,
}
struct Variant {
    title: String,
    upc: u64,
    sku: u64,
    images: VariantImages, // NOTE: should be in KABAB case
    dimensions: Dimensions,
    cost: f64,
    rr_price: f64,
    price: f64,
    qty: u64,
    stats: ProductStats,
}
struct Product {
    title: String,
    subtitle: String,
    slug: String,      // NOTE: should be in KABAB case
    thumbnail: String, // NOTE: should be in KABAB case
    video: String,
    description: String,
    brand_id: u64, // TODO: add Brand table
    options: Options,
    variants: Vec<Variant>,
    category: String,
    collection: Collection,
    tags: Vec<String>,
    status: Status,
}
impl Product {
    // add code here
}
